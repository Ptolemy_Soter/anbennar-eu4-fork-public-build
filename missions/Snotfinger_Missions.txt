g_snotfinger_column_4 = {
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = H89
		NOT = {
			has_country_modifier = allclan_split
		}
	}
	has_country_shield = yes

    g_the_runaway_slaves = {
		icon = mission_fortify_rajputana
		position = 1
		required_missions = {

		}
		trigger = {
			manpower = 40
			adm_power = 100
		}
		
		effect = {
			add_adm_power = -100
			custom_tooltip = snotfinger_purge_tooltip
			country_event = { 
				id = racial_pop_events_orcish.14
			}	
			largest_decrease_of_orcish_tolerance_effect = yes
			add_country_modifier = { 
				name = racial_pop_orcish_purge
				duration = -1  
			}
			custom_tooltip = racial_pop_events_debug.3.tooltip
			custom_tooltip = racial_pop_events_debug.4.tooltip
			hidden_effect = {
				every_owned_province = {
					limit = {
						has_small_orcish_minority_trigger = yes
					}
					add_unrest = 5
				}
				every_owned_province = {
					limit = {
						has_large_orcish_minority_trigger = yes
					}
					add_unrest = 7
				}
				every_owned_province = {
					limit = {
						has_orcish_majority_trigger = yes
					}
					add_unrest = 15
				}
			}
		}
	}
	g_the_silver_menace = {
		icon = mission_fortify_rajputana
		position = 2
		required_missions = {
			g_the_runaway_slaves
		}
		trigger = {
			any_known_country = {
				tag = H69
			}
		}
		
		effect = {
			add_casus_belli = {
				target = H69
				type = cb_insult
				months = 120
			}
		}
	}
	g_never_again = {
		icon = mission_fortify_rajputana
		position = 3
		required_missions = {
			g_the_silver_menace
		}
		trigger = {
			AND = {
				army_size = 30
				army_size = H72
				army_size = H80
			}
			OR = {
				any_known_country = {
					tag = H72
				}
				H72 = { exists = no }
			}
			OR = {
				any_known_country = {
					tag = H80
				}
				H80 = { exists = no }
			}
			
		}
		
		effect = {
			add_casus_belli = {
				target = H72
				type = cb_annex
				duration = 240
			}
			add_casus_belli = {
				target = H80
				type = cb_annex
				duration = 240
			}
		}
	}
	g_the_breakers_broken_and_the_crown_shattered = {
		icon = mission_fortify_rajputana
		position = 4
		required_missions = {
			g_never_again
		}
		trigger = {	
			H72 = { exists = no }
			H80 = { exists = no }
			owns = 4119
			owns = 4122
			owns = 4149
			owns = 4128
		}
		
		effect = {
			every_owned_province = {
				limit = {
					culture_group = orcish
				}
				change_culture = cave_goblin
				change_religion = goblin
			}
			4119 = {
				add_scaled_local_adm_power = 10
				add_scaled_local_dip_power = 10
				add_scaled_local_mil_power = 10
				add_base_tax = -5
				add_base_production = -5
				add_base_manpower = -5
			}
			4122 = {
				add_scaled_local_adm_power = 10
				add_scaled_local_dip_power = 10
				add_scaled_local_mil_power = 10
				add_base_tax = -5
				add_base_production = -5
				add_base_manpower = -5
			}
			4149 = {
				add_scaled_local_adm_power = 10
				add_scaled_local_dip_power = 10
				add_scaled_local_mil_power = 10
				add_base_tax = -5
				add_base_production = -5
				add_base_manpower = -5
			}
			4128 = {
				add_scaled_local_adm_power = 10
				add_scaled_local_dip_power = 10
				add_scaled_local_mil_power = 10
				add_base_tax = -5
				add_base_production = -5
				add_base_manpower = -5
			}
		}
	}
	g_the_warriors_with_words = {
		icon = mission_fortify_rajputana
		position = 5
		required_missions = {
			g_the_breakers_broken_and_the_crown_shattered
		}
		trigger = {
			power_projection = 99
		}
		
		effect = {
			add_country_modifier = {
				name = cave_goblin_snotfinger
				duration = -1
			}
			swap_free_idea_group = yes
			override_country_name = SLOZKLAN # Free Land
		}
	}
}


#these trees show up during the allclan civil war. Replaces generic resurce missions and special warband ones
g_snotfinger_civil_war_region_column_4 = { #involves doing stuff with their spawn area
	slot = 4
	generic = no
	ai = yes
	potential = {
		tag = H89
		has_country_modifier = allclan_split
	}
	has_country_shield = yes

    g_establishing_our_clan = {
		icon = mission_fortify_rajputana
		position = 1
		required_missions = {

		}
		trigger = {
			OR = {
				has_modifier = allclan_split
			}	
		}
		
		effect = {

		}
	}
}

g_snotfinger_civil_war_column_5 = { #involves fighting the civil war
	slot = 5
	generic = no
	ai = yes
	potential = {
		tag = H89
		has_country_modifier = allclan_split
	}
	has_country_shield = yes

    g_the_great_kinstrife = {
		icon = mission_fortify_rajputana
		position = 1
		required_missions = {

		}
		trigger = {
			OR = {
				has_modifier = allclan_split
			}	
		}
		
		effect = {
			create_general = {
				tradition = 100
				#add_siege = -1
			}
			override_country_name = SLOZKLAN # Free Land
			swap_free_idea_group = yes
		}
	}
}