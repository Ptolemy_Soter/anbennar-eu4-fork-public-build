defined_text = {
    name = strikeTheEarth
	random = no

    text = {
        localisation_key = strikeTheEarth_1
        trigger = {
            has_province_flag = strike_1
        }
    }
	text = {
        localisation_key = strikeTheEarth_2
        trigger = {
            has_province_flag = strike_2
        }
    }
	text = {
        localisation_key = strikeTheEarth_3
        trigger = {
            has_province_flag = strike_3
        }
    }
	text = {
        localisation_key = strikeTheEarth_4
        trigger = {
            has_province_flag = strike_4
        }
    }
	text = {
        localisation_key = strikeTheEarth_5
        trigger = {
            has_province_flag = strike_5
        }
    }
	text = {
        localisation_key = strikeTheEarth_6
        trigger = {
            has_province_flag = strike_6
        }
    }
	text = {
        localisation_key = strikeTheEarth_7
        trigger = {
            has_province_flag = strike_7
        }
    }
	text = {
        localisation_key = strikeTheEarth_8
        trigger = {
            has_province_flag = strike_8
        }
    }
	text = {
        localisation_key = strikeTheEarth_9
        trigger = {
            has_province_flag = strike_9
        }
    }
	text = {
        localisation_key = strikeTheEarth_10
        trigger = {
            has_province_flag = strike_10
        }
    }
	text = {
        localisation_key = strikeTheEarth_11
        trigger = {
            has_province_flag = strike_11
        }
    }
	text = {
        localisation_key = strikeTheEarth_12
        trigger = {
            has_province_flag = strike_12
        }
    }
	text = {
        localisation_key = strikeTheEarth_13
        trigger = {
            has_province_flag = strike_13
        }
    }
	text = {
        localisation_key = strikeTheEarth_14
        trigger = {
            has_province_flag = strike_14
        }
    }
	text = {
        localisation_key = strikeTheEarth_15
        trigger = {
            has_province_flag = strike_15
        }
    }
	text = {
        localisation_key = strikeTheEarth_16
        trigger = {
            has_province_flag = strike_16
        }
    }
	text = {
        localisation_key = strikeTheEarth_17
        trigger = {
            has_province_flag = strike_17
        }
    }
	text = {
        localisation_key = strikeTheEarth_18
        trigger = {
            has_province_flag = strike_18
        }
    }
	text = {
        localisation_key = strikeTheEarth_19
        trigger = {
            has_province_flag = strike_19
        }
    }
	text = {
        localisation_key = strikeTheEarth_20
        trigger = {
            has_province_flag = strike_20
        }
    }
}

defined_text = {
    name = swearAtTheEarth
	random = no

    text = {
        localisation_key = swearAtTheEarth_1
        trigger = {
            has_province_flag = swear_1
        }
    }
	text = {
        localisation_key = swearAtTheEarth_2
        trigger = {
            has_province_flag = swear_2
        }
    }
	text = {
        localisation_key = swearAtTheEarth_3
        trigger = {
            has_province_flag = swear_3
        }
    }
	text = {
        localisation_key = swearAtTheEarth_4
        trigger = {
            has_province_flag = swear_4
        }
    }
	text = {
        localisation_key = swearAtTheEarth_5
        trigger = {
            has_province_flag = swear_5
        }
    }
	text = {
        localisation_key = swearAtTheEarth_6
        trigger = {
            has_province_flag = swear_6
        }
    }
	text = {
        localisation_key = swearAtTheEarth_7
        trigger = {
            has_province_flag = swear_7
        }
    }
	text = {
        localisation_key = swearAtTheEarth_8
        trigger = {
            has_province_flag = swear_8
        }
    }
	text = {
        localisation_key = swearAtTheEarth_9
        trigger = {
            has_province_flag = swear_9
        }
    }
	text = {
        localisation_key = swearAtTheEarth_10
        trigger = {
            has_province_flag = swear_10
        }
    }
	text = {
        localisation_key = swearAtTheEarth_11
        trigger = {
            has_province_flag = swear_11
        }
    }
	text = {
        localisation_key = swearAtTheEarth_12
        trigger = {
            has_province_flag = swear_12
        }
    }
	text = {
        localisation_key = swearAtTheEarth_13
        trigger = {
            has_province_flag = swear_13
        }
    }
	text = {
        localisation_key = swearAtTheEarth_14
        trigger = {
            has_province_flag = swear_14
        }
    }
	text = {
        localisation_key = swearAtTheEarth_15
        trigger = {
            has_province_flag = swear_15
        }
    }
	text = {
        localisation_key = swearAtTheEarth_16
        trigger = {
            has_province_flag = swear_16
        }
    }
	text = {
        localisation_key = swearAtTheEarth_17
        trigger = {
            has_province_flag = swear_17
        }
    }
	text = {
        localisation_key = swearAtTheEarth_18
        trigger = {
            has_province_flag = swear_18
        }
    }
	text = {
        localisation_key = swearAtTheEarth_19
        trigger = {
            has_province_flag = swear_19
        }
    }
	text = {
        localisation_key = swearAtTheEarth_20
        trigger = {
            has_province_flag = swear_20
        }
    }
}

defined_text = {
    name = FederationStanding
	random = no

	text = {
        localisation_key = lake_is_dead
        trigger = {
            exists = no
        }
    }
	text = {
        localisation_key = under_foreign_occupation
        trigger = {
			NOT = { has_country_modifier = federation_fighter }
			NOT = { has_country_modifier = federation_subventionner }
            is_subject = yes
			overlord = { NOT = { has_country_modifier = lake_federation_member } }
        }
    }
	text = {
        localisation_key = under_foreign_occupation_sword
        trigger = {
			has_country_modifier = federation_fighter
            is_subject = yes
			overlord = { NOT = { has_country_modifier = lake_federation_member } }
        }
    }
	text = {
        localisation_key = under_foreign_occupation_purse
        trigger = {
			has_country_modifier = federation_subventionner
            is_subject = yes
			overlord = { NOT = { has_country_modifier = lake_federation_member } }
        }
    }
	text = {
        localisation_key = under_occupation
        trigger = {
			NOT = { has_country_modifier = federation_fighter }
			NOT = { has_country_modifier = federation_subventionner }
            is_subject = yes
        }
    }
	text = {
        localisation_key = under_occupation_sword
        trigger = {
			has_country_modifier = federation_fighter
            is_subject = yes
        }
    }
	text = {
        localisation_key = under_occupation_purse
        trigger = {
			has_country_modifier = federation_subventionner
            is_subject = yes
        }
    }
    text = {
        localisation_key = lake_leader
        trigger = {
			NOT = { has_country_modifier = federation_fighter }
			NOT = { has_country_modifier = federation_subventionner }
            has_country_flag = federation_leader
        }
    }
	text = {
        localisation_key = lake_leader_sword
        trigger = {
			has_country_modifier = federation_fighter
            has_country_flag = federation_leader
        }
    }
	text = {
        localisation_key = lake_leader_purse
        trigger = {
			has_country_modifier = federation_subventionner
            has_country_flag = federation_leader
        }
    }
	text = {
        localisation_key = lake_very_high_standing
        trigger = {
			NOT = { has_country_modifier = federation_fighter }
			NOT = { has_country_modifier = federation_subventionner }
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 4
				}
			}
        }
    }
	text = {
        localisation_key = lake_very_high_standing_sword
        trigger = {
			has_country_modifier = federation_fighter
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 4
				}
			}
        }
    }
	text = {
        localisation_key = lake_very_high_standing_purse
        trigger = {
			has_country_modifier = federation_subventionner
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 4
				}
			}
        }
    }
	text = {
        localisation_key = lake_high_standing
        trigger = {
			NOT = { has_country_modifier = federation_fighter }
			NOT = { has_country_modifier = federation_subventionner }
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 8
				}
			}
        }
    }
	text = {
        localisation_key = lake_high_standing_sword
        trigger = {
			has_country_modifier = federation_fighter
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 8
				}
			}
        }
    }
	text = {
        localisation_key = lake_high_standing_purse
        trigger = {
			has_country_modifier = federation_subventionner
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 8
				}
			}
        }
    }
	text = {
        localisation_key = lake_normal_standing
        trigger = {
			NOT = { has_country_modifier = federation_fighter }
			NOT = { has_country_modifier = federation_subventionner }
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 15
				}
			}
        }
    }
	text = {
        localisation_key = lake_normal_standing_sword
        trigger = {
			has_country_modifier = federation_fighter
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 15
				}
			}
        }
    }
	text = {
        localisation_key = lake_normal_standing_purse
        trigger = {
			has_country_modifier = federation_subventionner
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 15
				}
			}
        }
    }
	text = {
        localisation_key = lake_low_standing
        trigger = {
			NOT = { has_country_modifier = federation_fighter }
			NOT = { has_country_modifier = federation_subventionner }
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 25
				}
			}
        }
    }
	text = {
        localisation_key = lake_low_standing_sword
        trigger = {
			has_country_modifier = federation_fighter
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 25
				}
			}
        }
    }
	text = {
        localisation_key = lake_low_standing_purse
        trigger = {
			has_country_modifier = federation_subventionner
			NOT = {
				calc_true_if = {
					all_known_country = {
						check_variable = {
							which = FederationPoints
							which = PREV
						}
					}
					amount = 25
				}
			}
        }
    }
	text = {
        localisation_key = lake_very_low_standing
        trigger = {
			NOT = { has_country_modifier = federation_fighter }
			NOT = { has_country_modifier = federation_subventionner }
            always = yes
        }
    }
	text = {
        localisation_key = lake_very_low_standing_sword
        trigger = {
			has_country_modifier = federation_fighter
            always = yes
        }
    }
	text = {
        localisation_key = lake_very_low_standing_purse
        trigger = {
			has_country_modifier = federation_subventionner
            always = yes
        }
    }
}


defined_text = {
    name = FederationStatus
	random = no

    text = {
        localisation_key = federation_military
        trigger = {
            has_country_modifier = federation_fighter
        }
    }
	
	text = {
        localisation_key = federation_financer
        trigger = {
            has_country_modifier = federation_subventionner
        }
    }
	
	text = {
        localisation_key = federation_leecher
        trigger = {
			has_country_modifier = federation_leecher
        }
    }
}

defined_text = {
    name = FederationWarMandate
	random = no

    text = {
        localisation_key = have_war_mandate
        trigger = {
            has_country_modifier = federation_war_mandat
        }
    }
	
	text = {
        localisation_key = dont_have_war_mandate
        trigger = {
            NOT = { has_country_modifier = federation_war_mandat }
        }
    }
}

defined_text = {
    name = FederationLeague
	random = no

    text = {
        localisation_key = federation_blue_league
        trigger = {
            has_country_flag = federation_blue
        }
    }
	
	text = {
        localisation_key = federation_red_league
        trigger = {
            has_country_flag = federation_red
        }
    }
	
	text = {
        localisation_key = federation_yellow_league
        trigger = {
            has_country_flag = federation_yellow
        }
    }
	
	text = {
        localisation_key = federation_green_league
        trigger = {
            has_country_flag = federation_green
        }
    }
}

defined_text = {
    name = FederationTensionKodave
	random = no

    text = {
        localisation_key = federation_tension_kodave_resolute
        trigger = {
            REB = { check_variable = { kodave_resolve = 75 } }
        }
    }
	
	text = {
        localisation_key = federation_tension_kodave_eager
        trigger = {
            REB = { check_variable = { kodave_resolve = 50 } }
        }
    }
	
	text = {
        localisation_key = federation_tension_kodave_tense
        trigger = {
            REB = { check_variable = { kodave_resolve = 25 } }
        }
    }
	
	text = {
        localisation_key = federation_tension_kodave_calm
        trigger = {
            always = yes
        }
    }
}

defined_text = {
    name = FederationTensionEnuuk
	random = no

    text = {
        localisation_key = federation_tension_enuuk_resolute
        trigger = {
            REB = { check_variable = { enuuk_resolve = 75 } }
        }
    }
	
	text = {
        localisation_key = federation_tension_enuuk_eager
        trigger = {
            REB = { check_variable = { enuuk_resolve = 50 } }
        }
    }
	
	text = {
        localisation_key = federation_tension_enuuk_tense
        trigger = {
            REB = { check_variable = { enuuk_resolve = 25 } }
        }
    }
	
	text = {
        localisation_key = federation_tension_enuuk_calm
        trigger = {
            always = yes
        }
    }
}

defined_text = {
    name = FederationTensionYukel
	random = no

    text = {
        localisation_key = federation_tension_yukel_resolute
        trigger = {
            REB = { check_variable = { yukel_resolve = 75 } }
        }
    }
	
	text = {
        localisation_key = federation_tension_yukel_eager
        trigger = {
            REB = { check_variable = { yukel_resolve = 50 } }
        }
    }
	
	text = {
        localisation_key = federation_tension_yukel_tense
        trigger = {
            REB = { check_variable = { yukel_resolve = 25 } }
        }
    }
	
	text = {
        localisation_key = federation_tension_yukel_calm
        trigger = {
            always = yes
        }
    }
}

##########
##Lake Federation Constitution Loca
##########
defined_text = {
    name = FederationArticle1
	random = no
	
	text = {
        localisation_key = federation_mil_article_1
        trigger = {
            has_country_flag = federation_mil_article_1
        }
    }
	
	text = {
        localisation_key = federation_trade_article_1
        trigger = {
            has_country_flag = federation_trade_article_1
        }
    }
	
	text = {
        localisation_key = federation_industry_article_1
        trigger = {
            has_country_flag = federation_industry_article_1
        }
    }
}

defined_text = {
    name = FederationArticle2
	random = no
	
	text = {
        localisation_key = federation_mil_article_2
        trigger = {
            has_country_flag = federation_mil_article_2
        }
    }
	
	text = {
        localisation_key = federation_trade_article_2
        trigger = {
            has_country_flag = federation_trade_article_2
        }
    }
	
	text = {
        localisation_key = federation_industry_article_2
        trigger = {
            has_country_flag = federation_industry_article_2
        }
    }
}

defined_text = {
    name = FederationArticle3
	random = no
	
	text = {
        localisation_key = federation_mil_article_3
        trigger = {
            has_country_flag = federation_mil_article_3
        }
    }
	
	text = {
        localisation_key = federation_trade_article_3
        trigger = {
            has_country_flag = federation_trade_article_3
        }
    }
	
	text = {
        localisation_key = federation_industry_article_3
        trigger = {
            has_country_flag = federation_industry_article_3
        }
    }
}

defined_text = {
    name = FederationArticle4
	random = no
	
	text = {
        localisation_key = federation_mil_article_4
        trigger = {
            has_country_flag = federation_mil_article_4
        }
    }
	
	text = {
        localisation_key = federation_trade_article_4
        trigger = {
            has_country_flag = federation_trade_article_4
        }
    }
	
	text = {
        localisation_key = federation_industry_article_4
        trigger = {
            has_country_flag = federation_industry_article_4
        }
    }
}

defined_text = {
    name = FederationArticle5
	random = no
	
	text = {
        localisation_key = federation_mil_article_5
        trigger = {
            has_country_flag = federation_mil_article_5
        }
    }
	
	text = {
        localisation_key = federation_trade_article_5
        trigger = {
            has_country_flag = federation_trade_article_5
        }
    }
	
	text = {
        localisation_key = federation_industry_article_5
        trigger = {
            has_country_flag = federation_industry_article_5
        }
    }
}

defined_text = {
    name = FederationArticle6
	random = no
	
	text = {
        localisation_key = federation_mil_article_6
        trigger = {
            has_country_flag = federation_mil_article_6
        }
    }
	
	text = {
        localisation_key = federation_trade_article_6
        trigger = {
            has_country_flag = federation_trade_article_6
        }
    }
	
	text = {
        localisation_key = federation_industry_article_6
        trigger = {
            has_country_flag = federation_industry_article_6
        }
    }
}

defined_text = {
    name = FederationArticle7
	random = no
	
	text = {
        localisation_key = federation_mil_article_7
        trigger = {
            has_country_flag = federation_mil_article_7
        }
    }
	
	text = {
        localisation_key = federation_trade_article_7
        trigger = {
            has_country_flag = federation_trade_article_7
        }
    }
	
	text = {
        localisation_key = federation_industry_article_7
        trigger = {
            has_country_flag = federation_industry_article_7
        }
    }
}

defined_text = {
    name = FederationArticle8
	random = no
	
	text = {
        localisation_key = federation_mil_article_8
        trigger = {
            has_country_flag = federation_mil_article_8
        }
    }
	
	text = {
        localisation_key = federation_trade_article_8
        trigger = {
            has_country_flag = federation_trade_article_8
        }
    }
	
	text = {
        localisation_key = federation_industry_article_8
        trigger = {
            has_country_flag = federation_industry_article_8
        }
    }
}

defined_text = {
	name = KoboldHoard
	random = no
	
	text = {
		localisation_key = KoboldHoard1
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 75000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard2
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 60000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard3
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 50000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard4
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 40000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard5
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 30000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard6
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 25000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard7
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 20000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard8
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 15000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard9
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 10000 } }
		}
	}
	
	
	text = {
		localisation_key = KoboldHoard10
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 7500 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard11
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 0500 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard12
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 3000 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard13
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 1500 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard14
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 500 } }
		}
	}
	
	text = {
		localisation_key = KoboldHoard15
		trigger = {
			REB = { check_variable = { dragon_cult_hoard = 0 } }
		}
	}
}

defined_text = {
    name = CavernOfInterest
	random = no
	
	text = {
        localisation_key = CavernOfInterest_1
        trigger = {
            has_country_flag = cave_1
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_2
        trigger = {
            has_country_flag = cave_2
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_3
        trigger = {
            has_country_flag = cave_3
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_4
        trigger = {
            has_country_flag = cave_4
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_5
        trigger = {
            has_country_flag = cave_5
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_6
        trigger = {
            has_country_flag = cave_6
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_7
        trigger = {
            has_country_flag = cave_7
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_8
        trigger = {
            has_country_flag = cave_8
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_9
        trigger = {
            has_country_flag = cave_9
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_10
        trigger = {
            has_country_flag = cave_10
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_11
        trigger = {
            has_country_flag = cave_11
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_12
        trigger = {
            has_country_flag = cave_12
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_13
        trigger = {
            has_country_flag = cave_13
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_14
        trigger = {
            has_country_flag = cave_14
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_15
        trigger = {
            has_country_flag = cave_15
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_16
        trigger = {
            has_country_flag = cave_16
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_17
        trigger = {
            has_country_flag = cave_17
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_18
        trigger = {
            has_country_flag = cave_18
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_19
        trigger = {
            has_country_flag = cave_19
        }
    }
	
	text = {
        localisation_key = CavernOfInterest_20
        trigger = {
            has_country_flag = cave_20
        }
    }
}

defined_text = {
	name = MonstrousLevel
	random = no
	
	text = {
		localisation_key = MonstrousLevel75
		trigger = {
			check_variable = { monstrousPoints = 75 }
		}
	}
	
	text = {
		localisation_key = MonstrousLevel50
		trigger = {
			check_variable = { monstrousPoints = 50 }
		}
	}
	
	text = {
		localisation_key = MonstrousLevel25
		trigger = {
			check_variable = { monstrousPoints = 25 }
		}
	}
	
	text = {
		localisation_key = MonstrousLevel0
		trigger = {
			check_variable = { monstrousPoints = 0 }
		}
	}
}



###Ramsteel Mission Tree - Ramvault Expedition Custom Loca (should/can this go here?)
defined_text = {
    name = RamsteelExpeditionObstacle
    random = no

    text = {
        localisation_key = RamsteelExpeditionObstacleAmbush
        trigger = {
            has_country_flag = ramsteel_goblin_ambush
        }
    }
    text = {
        localisation_key = RamsteelExpeditionObstacleCurse
        trigger = {
            has_country_flag = ramsteel_ancient_curse
        }
    }
    text = {
        localisation_key = RamsteelExpeditionObstacleCollapse
        trigger = {
            has_country_flag = ramsteel_cave_collapse
        }
    }
}

defined_text = {
    name = RamsteelExpeditionObstacleSuccess
    random = no

    text = {
        localisation_key = RamsteelExpeditionObstacleAmbushSuccess
        trigger = {
            has_country_flag = ramsteel_goblin_ambush
        }
    }
    text = {
        localisation_key = RamsteelExpeditionObstacleCurseSuccess
        trigger = {
            has_country_flag = ramsteel_ancient_curse
        }
    }
    text = {
        localisation_key = RamsteelExpeditionObstacleCollapseSuccess
        trigger = {
            has_country_flag = ramsteel_cave_collapse
        }
    }
}

defined_text = {
    name = RamsteelExpeditionObstacleFail
    random = no

    text = {
        localisation_key = RamsteelExpeditionObstacleAmbushFail
        trigger = {
            has_country_flag = ramsteel_goblin_ambush
        }
    }
    text = {
        localisation_key = RamsteelExpeditionObstacleCurseFail
        trigger = {
            has_country_flag = ramsteel_ancient_curse
        }
    }
    text = {
        localisation_key = RamsteelExpeditionObstacleCollapseFail
        trigger = {
            has_country_flag = ramsteel_cave_collapse
        }
    }
}


### Sugamber Mission Tree
defined_text = {
    name = SugamberRhinmondTroupe
    random = no

    text = {
        localisation_key = SugamberRhinmondTroupeFirst
        trigger = {
            NOT = { has_country_flag = sugamber_rhinmond_troupe_fired }
        }
    }
    text = {
        localisation_key = SugamberRhinmondTroupeSubsequent
        trigger = {
            has_country_flag = sugamber_rhinmond_troupe_fired
        }
    }
}

### Konwell Mission Tree
defined_text = {
    name = KonwellRyalanRefuses
    random = no
    text = {
        localisation_key = KonwellRyalanRefusesElector
        trigger = {
            A43 = {
                is_elector = yes
            }
        }
    }
    text = {
        localisation_key = KonwellRyalanRefusesEmperor
        trigger = {
            A43 = {
                is_emperor = yes
            }
        }
    }
    text = {
        localisation_key = KonwellRyalanRefusesVassal
        trigger = {
            A43 = {
                is_vassal = yes
            }
        }
    }
    text = {
        localisation_key = KonwellRyalanRefusesReligion
        trigger = {
            A43 = {
                has_matching_religion = A35
            }
        }
    }
}